# YleTTV

Python pohjainen CLI-ohjelma, jolla voi selata Ylen teksti TV:tä heidän API:n kautta.

### Asennus

Lataa ensin repositorio git clonella tai lataamalla lähdekoodi suoraan

```
$ git clone https://gitlab.com/Iusbus/ylettv.git
```

Tämän jälkeen, tarvitset oman API avaimesi Yleltä. Ohjeet [täältä](https://developer.yle.fi/)

Kun olet löytänyt API avaimesi, luo tiedosto samaan kansioon missä python skripti (ylettv.py) sijaitsee,
ja kopioi avain siihen. Nimeä tiedosto **.api_key**.

### Käyttö

Aloita käyttö komennolla:

```
$ python3 ylettv
```

Ohjelma tulostaa (mikäli se saa yhteyden rajapintaan) automaattisesti sivun 199, päähakemiston.

```
                                    1/4
      Päähakemistojen kooste
 101 UUTISET         112 Vaaratiedotteet
 100 Etusivu         175 Pörssisivut
 102 Kotimaa         190 News in English
 130 Ulkomaat    196-198 Aakkosellinen
 160 Talous              hakemisto
 200 URHEILU     297-299 Moottoriurheilu
 201 Urheilu-uutiset 460 Ravit
 220 Palloilusarjat  470 Veikkaus
 670 Eurojalkapallo  600 Suurtapahtumat
 500 ALUEUUTISET           513 P-Pohjanm
 501 E-Karj. 507 Kymenlaak 514 P-Savo
 502 E-Savo  508 Lappi     515 Päijät-Hä
 503 Häme    509 Meri-Lapp 516 Helsinki/
 504 Kainuu  510 Pirkanmaa     Uusimaa
 505 K-Pohj. 511 Pohjanmaa 517 Satakunta
 506 K-Suomi 512 P-Karjala 518 Vars-Suom
     Uutiset saameksi:     519 E-Pohjanm
     520 pohjoissaami  521 inarinsaami

...
...
...

Valitse sivu:
```

Yksinkertaisesti kirjoita sivunumero, jolloin ohjelma tulostaa halutun sivun.

Lopettaakesi käytön, älä syötä mitään, ja paina vain enteriä.
