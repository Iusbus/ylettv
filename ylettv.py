import os
import re
import requests
from json import JSONDecodeError

class FetchFail(Exception):
    pass

def getAPI():
    try:
        api_file=open(os.path.dirname(__file__) + '/.api_key','r').read()
    except IOError:
        print("Tiedostoa .api_key ei löytynyt")
    else:
        keys=re.match('app_id=(.*)&app_key=(.*)',api_file)
        global api_id
        global api_key
        api_id=keys.group(1)
        api_key=keys.group(2)

def fetch(page):
    request = requests.get(f'https://external.api.yle.fi/v1/teletext/pages/{page}.json?app_id={api_id}&app_key={api_key}',headers={'Accept-Encoding':'gzip'})
    if request.status_code == 401:
        raise FetchFail(f"Ei pääsyoikeutta sivulle {page}, API joko rajoitettu tai API avain on väärä")
    elif request.status_code == 404:
        raise FetchFail(f"Sivua {page} ei löytynyt")
    else:
        try:
            data = request.json()
        except JSONDecodeError:
            raise FetchFail("Sivu ei ole sopivassa JSON muodossa")
        else:
            os.system('cls' if os.name == 'nt' else 'clear')
            pagePrint(page,data)

def pagePrint(page,data):
    for subpage in data["teletext"]["page"]["subpage"]:
        for line in subpage["content"][0]["line"]:
            try:
                print(line["Text"])
            except:
                pass

def main():
    try:
        getAPI()
        fetch('199')
    except NameError:
        pass
    except Exception as errorMsg:
        print(errorMsg)
    else:
        while True:
            toFetch = input("\nValitse sivu: ")
            if toFetch == "":
                break
            else:
                try:
                    int(toFetch)
                    fetch(toFetch)
                except ValueError:
                    print('Syöte ei ollut numero')
                except Exception as errorMsg:
                    print(errorMsg)

main()
